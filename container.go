package main
import "os"
import "os/exec"
import "path/filepath"
import "io/ioutil"
import "strconv"
import "syscall"
import "fmt"

func main(){
	switch os.Args[1] {
	case "run":
		run()
        case "child":
                child()
	default:
		panic("bad command")
	}
}

func run(){
	fmt.Printf("Running %v as %d\n", os.Args[1:], os.Getpid())

	cmd := exec.Command("/proc/self/exe", append([]string{"child"}, os.Args[2:]...)...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.SysProcAttr = &syscall.SysProcAttr{
		Cloneflags: syscall.CLONE_NEWUTS | syscall.CLONE_NEWPID | syscall.CLONE_NEWNS,
		Unshareflags: syscall.CLONE_NEWNS,
	}
	cmd.Run()
}

func child(){
        fmt.Printf("Running %v as %d\n", os.Args[1:], os.Getpid())

	cg()
	syscall.Sethostname([]byte("AbysUbuntuContainer"))
	syscall.Chroot("/var/lib/docker/overlay2/c7e95c272dd0b3cb1a45212d1d6a49ca4adac24f3569839341c9fd329eb3c6f4/diff")
	syscall.Chdir("/")
	syscall.Mount("proc", "proc", "proc", 0, "")

	cmd := exec.Command(os.Args[2], os.Args[3:]...)
        cmd.Stdin = os.Stdin
        cmd.Stdout = os.Stdout
        cmd.Stderr = os.Stderr
        cmd.Run()

	syscall.Unmount("/proc", 0)
}

func cg(){
        cgroupbase := "/sys/fs/cgroup"
	pidsbase := filepath.Join(cgroupbase, "pids")
	/*
	memorybase := filepath.Join(cgroupbase, "memory")
	blkiobase := filepath.Join(cgroupbase, "blkio")
	cpuacctbase := filepath.Join(cgroupbase, "cpu,cpuacct")
	cpusetbase := filepath.Join(cgroupbase, "cpuset")
	devicesbase := filepath.Join(cgroupbase, "devices")
	freezerbase := filepath.Join(cgroupbase, "freezer")
	hugetlbbase := filepath.Join(cgroupbase, "hugetlb")
	net_clsbase := filepath.Join(cgroupbase, "net_cls")
	perf_eventbase := filepath.Join(cgroupbase, "perf_event")
	systemdbase := filepath.Join(cgroupbase, "systemd")
	*/

	err := os.Mkdir(filepath.Join(pidsbase, "abys"), 0755)
	if err != nil && !os.IsExist(err){
		panic(err)
	}

	must(ioutil.WriteFile(filepath.Join(pidsbase, "abys/cgroup.procs"),[]byte(strconv.Itoa(os.Getpid())),0700))

}

func must(err error){
	if err != nil {
		panic(err)
	}
}
